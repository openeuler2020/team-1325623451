#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sysinfo.h>

#define  __USE_GNU
#include <sched.h>
#include <pthread.h>
#define  THREAD_MAX_NUM 200  //the most threads on 1 CPU
#define	 MINLOAD 20		//minimum randomload or pulseload(less than 10 may cause some time error in random/pulse)

void * threadfunction(void* param);
void lineload(void);
void trapezoidload(void);
void sinload(void);
void randomload(void);
void rand_lineload(long long ptime,int pload);
void pulseload(void);
void pulse_happen(int pload);
void pulse_wait(int duration,int normalload);
//void set_action(char * action[]);

int num = 0;    //cpu's cores
int operation = 0;	//default 0;
int m   = 50;   //default 50, for the first input parameter
int n   = 20;	//defualt 20, for the second input parameter
int p_time = 1000;  //defualt 1 second
int p_load = 50;    //defualt 50% cpuload
int randommessage = 0;	//simultaneous random

int main(int argc, char * argv[])
{
    int tid[THREAD_MAX_NUM];
    int i;
    pthread_t thread[THREAD_MAX_NUM];

    num = sysconf(_SC_NPROCESSORS_ONLN);  //get the number of cores for creating thread

    //set_action();
    if(argv[1][0] == 'l')
    {
    	printf("executing lineload program\n");
		m = atoi(argv[2]);  //m% cpuload
		operation = 1;
    }
    else if(argv[1][0] == 'r')
    {
    	printf("executing randomload program\n");
    	m = atoi(argv[2]);  //time of duration(second)
    	operation = 2;
    }
    else if(argv[1][0] == 't')
    {
    	printf("executing trapezoidload program\n");
    	m = atoi(argv[2]);  //baseload
    	n = atoi(argv[3]);  //increase per step
    	operation = 3;
    }
    else if(argv[1][0] == 's')
    {
    	printf("executing sinload program\n");
    	m = atoi(argv[2]);  //baseload
    	n = atoi(argv[3]);  //fluctuating value
    	operation = 4;
    }
    else if(argv[1][0] == 'p')
    {
    	printf("executing pulseload program\n");
    	m = atoi(argv[2]);	//normalload time
    	n = atoi(argv[3]);	//normalload m% for no pulse
    	operation = 5;
    }
    else
    {
    	printf("wrong param\n");
		exit(-1);
    }

    for(i=0;i<num;i++)
    {
           tid[i] = i;
           pthread_create(&thread[i],NULL,threadfunction,(void*)&tid[i]);
    }
    for(i=0; i< num; i++)
        pthread_join(thread[i],NULL);	//wait all thread for default and should use ctrl+c to stop
    return 0;
}

void * threadfunction(void* param)
{
    cpu_set_t mask;

    int *cpu_num = (int *)param;
    CPU_ZERO(&mask);    //put zero
    CPU_SET(*cpu_num, &mask);   //set mask
    if (sched_setaffinity(0, sizeof(mask), &mask) == -1)    //set thread on CPU
           printf("warning: could not set CPU affinity, continuing...\n");

    cpu_set_t get;
    int i;
    CPU_ZERO(&get);
    if (sched_getaffinity(0, sizeof(get), &get) == -1)  //get mask
            printf("warning: cound not get thread affinity, continuing...\n");
    for (i = 0; i < num; i++)   //for debug
            if (CPU_ISSET(i, &get))
                     printf("this thread %d is running processor : %d\n", i,i);

    switch(operation)
    {
    	case 1:
    		lineload();
    		break;
    	case 2:
    		randomload();
    		break;
    	case 3:
    		trapezoidload();
    		break;
    	case 4:
    		sinload();
    		break;
    	case 5:
    		pulseload();
    		break;
    	default:
    	{
    		printf("wrong operation\n");
			exit(-1);
    	}
    }

    return NULL;
}

void lineload(void)
{   

    struct timespec timer1 = {0,0};
    struct timespec timer2 = {0,0};

    while(1)
    {
        unsigned int busy  = m * 10;
        unsigned int relax = 1000 - busy;
        usleep(relax*1000);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer1);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2);
        while((timer2.tv_sec-timer1.tv_sec)*1000*1000*1000+timer2.tv_nsec - timer1.tv_nsec < busy*1000*1000)
            clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2);

    }
    return;
}

void randomload(void)  //part:the work contains p part.t_overall:the work cost t s.
{
    srand((unsigned)time(NULL));
    int pload;
    p_time = m*1000;

    cpu_set_t get;
    CPU_ZERO(&get);

    while(1)
    {
        pload = rand()%(100-MINLOAD)+MINLOAD;

        sched_getaffinity(0, sizeof(get), &get);
        if(CPU_ISSET(0, &get))  //use CPU0 to make random number
        {
        	//randommessage = 0;
        	p_load = pload;
        }

        printf("ptime:%d, pload:%d\n",p_time,p_load);

        rand_lineload(p_time,p_load);
        while(randommessage != 0)
    		usleep(1);
    }
    
    return;
}

void rand_lineload(long long ptime,int pload)  //ptime:the time of this part. pload:the load of this part.
{

    struct timespec timer1 = {0,0};
    struct timespec timer2 = {0,0};
    struct timespec start = {0,0};
    struct timespec later = {0,0};
    clock_gettime(CLOCK_THREAD_CPUTIME_ID,&start);
    clock_gettime(CLOCK_THREAD_CPUTIME_ID,&later);


    while((long long)((later.tv_sec-start.tv_sec)*1000*1000*1000 + later.tv_nsec - start.tv_nsec) < ptime*1000*1000)
    {

        unsigned int busy  = pload * 10;
        unsigned int relax = 1000 - busy;
        usleep(relax*1000);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer1);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2);
        while((timer2.tv_sec-timer1.tv_sec)*1000*1000*1000+timer2.tv_nsec - timer1.tv_nsec < busy*1000*1000)
            clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&later);

    }

    randommessage++;
    randommessage %= 8;

    return;
}

void trapezoidload(void)
{
    struct timespec timer1 = {0,0};
    struct timespec timer2 = {0,0};

    int length = 100/n;
    int i,j,k;
    int a[2*length + 1];
    j = 0;
    m %= n;	//from the smallest load start
    for (i = 0; i < length; ++i)
    {
        a[i] = a[2*length-i] = m;
        m 	 = m + n;
    }
    a[length] = a[length-1];
    while(1)
    {
        i = 25;
        j++;
        k = j % (2*length+1);
        m=a[k];
        while(i-->0)
        {
            unsigned int busy  = m * 10;
            unsigned int relax = 1000 - busy;
            usleep(relax*1000);
            clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer1);
            clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2);
            while((timer2.tv_sec-timer1.tv_sec)*1000*1000*1000 + timer2.tv_nsec - timer1.tv_nsec < busy*1000*1000)
                clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2);
        }
    }
    return;
}

void sinload(void)
{
    struct timespec timer1 = {0,0};
    struct timespec timer2 = {0,0};
    const double pi = 3.1415926;
    //将一个正弦周期等分为100，使每一个时间常量为2*pi/100(we cannot explain it in english)
    unsigned int currentTimeCount = 0;
    while(1)
    {
        unsigned int busy  = sin(currentTimeCount*2*pi/100)*10*n + 10*m;
        unsigned int relax = 1000 - busy;
        usleep(relax*1000);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer1);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2);
        while((timer2.tv_sec-timer1.tv_sec)*1000*1000*1000+timer2.tv_nsec - timer1.tv_nsec < busy*1000*1000)
            clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2);

        currentTimeCount++;
        currentTimeCount %= 100;
    }
    return;
}



void pulseload(void)  //part:the work contains p part.t_overall:the work cost t s.
{
    srand((unsigned)time(NULL));
    int duration = m*1000; //s to ms
    int normalload = n;
    int pload;
    cpu_set_t get;
    CPU_ZERO(&get);

    while(1)
    {
        pload = rand()%(100-MINLOAD)+MINLOAD;

        sched_getaffinity(0, sizeof(get), &get);
        if(CPU_ISSET(0, &get))  //use CPU0 to make random number
        {
        	//randommessage = 0;
        	p_load = pload;
        }

        //printf("duration=%d,start\n",duration);
        pulse_wait(duration,normalload);
        //printf("duration,end\n");

        //printf("pulse happen,load=%d,start\n",pload);
        pulse_happen(p_load);
        //printf("pulse,end\n");
    }

    return;
}

void pulse_wait(int duration,int normalload) 
{

    struct timespec timer1_wait = {0,0};
    struct timespec timer2_wait = {0,0};
    struct timespec start_wait = {0,0};
    struct timespec later_wait = {0,0};
    clock_gettime(CLOCK_THREAD_CPUTIME_ID,&start_wait);
    clock_gettime(CLOCK_THREAD_CPUTIME_ID,&later_wait);

    while((later_wait.tv_sec-start_wait.tv_sec)*1000 + (later_wait.tv_nsec/1000000) - (start_wait.tv_nsec/1000000) < duration)
    {

        unsigned int busy  = normalload*10;
        unsigned int relax = 1000 - busy;
        usleep(relax*1000);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer1_wait);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2_wait);
        while((timer2_wait.tv_sec-timer1_wait.tv_sec)*1000+timer2_wait.tv_nsec/1000000 - timer1_wait.tv_nsec/1000000 < busy)
            clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2_wait);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&later_wait);
    }
    return;
}


void pulse_happen(int pload) 
{
    //clock_t start = clock();

    struct timespec timer1_happen = {0,0};
    struct timespec timer2_happen = {0,0};
    struct timespec start_happen = {0,0};
    struct timespec later_happen = {0,0};
    clock_gettime(CLOCK_THREAD_CPUTIME_ID,&start_happen);
    clock_gettime(CLOCK_THREAD_CPUTIME_ID,&later_happen);

    //printf("the pulse load is %d\n",pload);

    while((later_happen.tv_sec-start_happen.tv_sec)*1000 + later_happen.tv_nsec/1000000 - start_happen.tv_nsec/1000000 < 20*pload)
    {
        clock_t start = clock();
        //printf("start at ")
        unsigned int busy  = pload * 10;
        unsigned int relax = 1000 - busy;
        usleep(relax*1000);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer1_happen);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2_happen);
        while((timer2_happen.tv_sec-timer1_happen.tv_sec)*1000+timer2_happen.tv_nsec/1000000 - timer1_happen.tv_nsec/1000000 < busy)
            clock_gettime(CLOCK_THREAD_CPUTIME_ID,&timer2_happen);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&later_happen);
        //clock_t end = clock();
        //printf("time = %ld\n",end-start);
    }
    return;
}